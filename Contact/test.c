#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"
//通讯录菜单
void menu()
{
	printf("******************通讯录*******************\n");
	printf("*********1.添加联系人 2.删除联系人*********\n");
	printf("*********3.修改联系人 4.查找联系人*********\n");
	printf("*********5.展示联系人 0.   退出  **********\n");
	printf("*******************************************\n");

}
int main()
{
	int option = -1;
	Contact con;
	ContactInit(&con);
	do
	{
		menu();
		printf("请选择您的操作:\n");
		scanf("%d", &option);
		switch (option)
		{
		case 1:
			ContactAdd(&con);
			break;
		case 2:
			ContactDel(&con);
			break;
		case 3:
			ContactModify(&con);
			break;
		case 4:
			ContactFind(&con);
			break;
		case 5:
			ContactShow(&con);
			break;
		case 0:
			printf("退出通讯录！\n");
			break;
		default:
			printf("输入错误，请重新输入！\n");
			break;
		}
	} while (option != 0);
	ContactDestory(&con);
	return 0;
}