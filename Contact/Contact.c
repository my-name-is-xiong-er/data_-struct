#define _CRT_SECURE_NO_WARNINGS 1
#include"Contact.h"
#include"SeqList.h"
//保存联系人数据
void ContactSave(Contact* con)
{
	//打开文件
	FILE* Cpf = fopen("contact.txt", "wb");
	if (Cpf == NULL)
	{
		perror("fopen fail!");
		return 1;
	}
	//循环保存联系人数据到文件中
	for (int i = 0; i < con->size; i++)
	{
		fprintf(Cpf, "%s %s %d %s %s\n", con->arr[i].name,
			con->arr[i].gender,
			con->arr[i].age,
			con->arr[i].tel,
			con->arr[i].addr
		);
	}
	printf("通讯录数据保存成功！\n");
	//关闭文件
	fclose(Cpf);
	Cpf = NULL;
}
//导入联系人数据到通讯录中
void ContactLoad(Contact* con)
{
	//打开文件
	FILE* Cpf = fopen("contact.txt", "rb");
	if (Cpf == NULL)
	{
		perror("fopen fail!");
		return 1;
	}
	//循环导入文件数据
	perInfo info;
	while (fscanf(Cpf, "%s %s %d %s %s", info.name, info.gender, &(info.age), info.tel, info.addr) != EOF)
	{
		SLPushBack(con, info);
	}
	printf("历史通讯录数据导入成功！\n");
	//关闭文件
	fclose(Cpf);
	Cpf = NULL;
}
void ContactInit(Contact* con)
{
	//通讯录的初始化即顺序表的初始化
	SLInit(con);
	//将历史数据导入通讯录中
	ContactLoad(con);
}
void ContactAdd(Contact* con)
{
	perInfo info;
	printf("请输入联系人的姓名:\n");
	scanf("%s", info.name);

	printf("请输入联系人的性别:\n");
	scanf("%s", info.gender);

	printf("请输入联系人的年龄:\n");
	scanf("%d", &info.age);

	printf("请输入联系人的电话:\n");
	scanf("%s", info.tel);

	printf("请输入联系人的地址:\n");
	scanf("%s", info.addr);

	SLPushBack(con, info);
}
int FindByName(Contact* con, char name[])
{
	for (int i = 0; i < con->size; i++)
	{
		if (0 == strcmp(con->arr[i].name, name))
		{
			//找到啦
			return i;
		}
	}
	//没有找到
	return -1;
}
void ContactDel(Contact* con)
{
	//删除联系人之前要确保要删除的联系人要存在
	//可以通过姓名||性别||年龄||电话||地址来查找要删除的联系人
	char name[NAME_MAX];
	printf("请输入要删除的联系人姓名:\n");//通过姓名查找联系人
	scanf("%s", name);
	int find = FindByName(con, name);
	if (find < 0)
	{
		printf("要删除的联系人不存在！\n");
		return;
	}
	//找到了要删除的联系人-->下标为find
	SLErase(con, find);
	printf("删除成功！\n");
}
void ContactShow(Contact* con)
{
	//先打印表头
	printf("%s %8s %8s %8s %8s\n", "姓名", "性别", "年龄", "电话", "地址");
	//逐个打印联系人数据
	for (int i = 0; i < con->size; i++)
	{
		printf("%-9s %-8s %-7d %-8s %-s\n", con->arr[i].name,
			con->arr[i].gender,
			con->arr[i].age,
			con->arr[i].tel,
			con->arr[i].addr
		);
	}
}
void ContactModify(Contact* con)
{
	//修改联系人数据之前要确保要修改的联系人数据存在
	char name[NAME_MAX];
	printf("请输入要修改的联系人姓名:\n");
	scanf("%s", name);
	int find = FindByName(con, name);
	if (find < 0)
	{
		printf("要修改的联系人数据不存在！\n");
		return;
	}
	//要修改的联系人数据存在
	printf("请输入新的姓名:\n");
	scanf("%s", con->arr[find].name);
	printf("请输入新的性别:\n");
	scanf("%s", con->arr[find].gender);
	printf("请输入新的年龄:\n");
	scanf("%d", &con->arr[find].age);
	printf("请输入新的电话:\n");
	scanf("%s", con->arr[find].tel);
	printf("请输入新的地址:\n");
	scanf("%s", con->arr[find].addr);
	printf("修改成功！\n");
}
void ContactFind(Contact* con)
{
	char name[NAME_MAX];
	printf("请输入要查找的联系人姓名:\n");
	scanf("%s", name);
	int find = FindByName(con, name);
	if (find < 0)
	{
		printf("要查找的联系人数据不存在！\n");
		return;
	}
	//打印联系人数据
	printf("%s %8s %8s %8s %8s\n", "姓名", "性别", "年龄", "电话", "地址");
	printf("%-9s %-8s %-7d %-8s %-s\n", con->arr[find].name,
		con->arr[find].gender,
		con->arr[find].age,
		con->arr[find].tel,
		con->arr[find].addr
	);
}
void ContactDestory(Contact* con)
{
	//销毁通讯录之前要先保存通讯录中的数据，以防丢失
	ContactSave(con);
	//销毁通讯录
	SLDestory(con);
}