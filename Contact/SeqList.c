#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"
void SLInit(SL* ps)
{
	ps->arr = NULL;
	ps->size = 0;
	ps->capacity = 0;
}
void SLCheckCapacity(SL* ps)
{
	if (ps->size == ps->capacity)
	{
		int newCapacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;//判断空间是否够用
		SLDataType* tmp = (SLDataType*)realloc(ps->arr, newCapacity * sizeof(SLDataType));//要申请多大的空间
		if (tmp == NULL)
		{
			perror("realloc fail!");
			exit(1);
		}
		ps->arr = tmp;//确定动态开辟空间成功，再将指针赋给ps->arr
		ps->capacity = newCapacity;
	}
}
void SLPushBack(SL* ps, SLDataType x)
{
	assert(ps);
	//插入数据之前先检查空间够不够用，不够需要增容
	SLCheckCapacity(ps);
	ps->arr[ps->size++] = x;
}
void SLPushFront(SL* ps, SLDataType x)
{
	assert(ps);
	//插入数据之前先检查空间够不够用，不够需要增容
	SLCheckCapacity(ps);
	for (int i = ps->size; i > 0; i--)
	{
		ps->arr[i] = ps->arr[i - 1]; //当到ps->arr[1]=ps->arr[0]时挪动完毕，则i>0
	}
	ps->arr[0] = x;
	ps->size++;//头插数据以后记得要让size自增1
}
void SLPopBack(SL* ps)
{
	assert(ps);
	//尾删之前要确保有效数据个数不为0
	assert(ps->size != 0);
	ps->size--;
}
void SLPopFront(SL* ps)
{
	assert(ps);
	//头删之前要确保有效数据的个数不为0
	assert(ps->size != 0);
	for (int i = 0; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];//当ps->arr[size-2]=ps->arr[size-1]时完毕，所以i<ps->size-1
	}
	ps->size--;
}
void SLInsert(SL* ps, int pos, SLDataType x)
{
	assert(ps);
	//在指定位置插入数据，要考边界位置处
	assert(pos >= 0 && pos <= ps->size);
	//插入的时候先检查空间够不够
	SLCheckCapacity(ps);
	for (int i = ps->size; i > pos; i--)
	{
		ps->arr[i] = ps->arr[i - 1];//当ps->arr[pos+1]=ps->arr[pos]时完毕
	}
	ps->arr[pos] = x;
	ps->size++;
}
void SLErase(SL* ps, int pos)
{
	assert(ps);
	//指定的位置要在数据下标的有效范围以内
	assert(pos >= 0 && pos < ps->size);
	for (int i = pos; i < ps->size - 1; i++)
	{
		ps->arr[i] = ps->arr[i + 1];//当ps->arr[size-2]=ps->arr[size-1]时完毕
	}
	ps->size--;
}
void SLDestory(SL* ps)
{
	if (ps->arr != NULL)
	{
		free(ps->arr);
		ps->arr = NULL;
	}
	ps->size = 0;
	ps->capacity = 0;
}