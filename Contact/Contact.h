#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#define NAME_MAX 20
#define GENDER_MAX 10
#define TEL_MAX 20
#define ADDR_MAX 100
//定义一个结构体用来描述联系人的个人信息
typedef struct personInfo
{
	char name[NAME_MAX];
	char gender[GENDER_MAX];
	int age;
	char tel[TEL_MAX];
	char addr[ADDR_MAX];
}perInfo;

//前置声明(因为顺序表的定义在SeqList.h中,而头文件不能相互包含,所以要进行结构体的前置声明)
typedef struct SeqList Contact;

//1.通讯录的初始化
void ContactInit(Contact* con);

//2.通讯录联系人的添加
void ContactAdd(Contact* con);

//3.通讯录联系人的删除
void ContactDel(Contact* con);

//4.展示通讯录数据
void ContactShow(Contact* con);

//5.通讯录的修改
void ContactModify(Contact* con);

//6.查找联系人
void ContactFind(Contact* con);

//7.通讯录的销毁
void ContactDestory(Contact* con);