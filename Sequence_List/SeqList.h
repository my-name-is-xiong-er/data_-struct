#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
//静态顺序表
//typedef int SLDataType;
//#define N 10
//typedef struct SeqList
//{
//	SLDataType arr[N];
//	int size;  //有效数据个数
//}SL;

//动态顺序表
typedef int SLDataType;
typedef struct SeqList
{
	SLDataType* arr;
	int size; //有效数据个数
	int capacity;//空间容量
}SL;

//1.顺序表的初始化
void SLInit(SL* ps);

//2.检查空间是否够用
void SLCheckCapacity(SL* ps);

//3.打印顺序表中arr的内容
void SLPrint(SL sl);

//4.顺序表的尾插(从有效数据的后面插入数据)
void SLPushBack(SL* ps, SLDataType x);

//5.顺序表的头插(从arr指向的起始位置开始插入数据)
void SLPushFront(SL* ps, SLDataType x);

//6.顺序表的尾删
void SLPopBack(SL* ps);

//7.顺序表的头删
void SLPopFront(SL* ps);

//8.在指定位置之前插入数据
void SLInsert(SL* ps, int pos, SLDataType x);

//9.删除指定位置的数据
void SLErase(SL* ps, int pos);

//10.顺序表的查找
int SLFind(SL* ps, SLDataType x);

//11.顺序表的销毁
void SLDestory(SL* ps);
