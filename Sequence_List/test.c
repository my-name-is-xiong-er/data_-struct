#define _CRT_SECURE_NO_WARNINGS 1
#include"SeqList.h"
void SLTest01()
{
	SL sl;
	//初始化顺序表
	SLInit(&sl);
	//尾插
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(sl);
	//头插
	SLPushFront(&sl, 5);
	SLPrint(sl);
	SLPushFront(&sl, 6);
	SLPrint(sl);
	//尾删
	SLPopBack(&sl);
	SLPrint(sl);
	SLPopBack(&sl);
	SLPrint(sl);
	//头删
	SLPopFront(&sl);
	SLPrint(sl);
	SLPopFront(&sl);
	SLPrint(sl);
	SLPopFront(&sl);
	SLPrint(sl);
	SLPopFront(&sl);
	SLPrint(sl);
	//销毁顺序表
	SLDestory(&sl);
}
void SLTest02()
{
	SL sl;
	SLInit(&sl);
	//尾插
	SLPushBack(&sl, 1);
	SLPushBack(&sl, 2);
	SLPushBack(&sl, 3);
	SLPushBack(&sl, 4);
	SLPrint(sl);
	//在指定的位置之前插入数据
	SLInsert(&sl, 0, 99);
	SLPrint(sl);
	SLInsert(&sl, sl.size, 100);
	SLPrint(sl);
	SLInsert(&sl, 2, 98);
	SLPrint(sl);
	//删除指定位置的数据
	SLErase(&sl, 0);
	SLPrint(sl);
	SLErase(&sl, 2);
	SLPrint(sl);
	//顺序表的查找
	int find = SLFind(&sl, 4);
	if (find < 0)
	{
		printf("没有找到！\n");
	}
	else
	{
		printf("找到了，下标为%d\n", find);
	}
	//销毁顺序表
	SLDestory(&sl);
}
int main()
{
	SLTest01();
	//SLTest02();
	return 0;
}
