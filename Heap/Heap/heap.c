#include"heap.h"

void HeapInit(HP* php)
{
	assert(php != NULL);
	php->arr = NULL;
	php->size = php->capacity = 0;
}

void Swap(HPDataType* p1, HPDataType* p2)
{
	HPDataType tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

//向上调整算法
void AdjustUp(HPDataType* arr, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);

			child = parent;
			parent = (child - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

//向下调整算法
void AdjustDown(HPDataType* arr, int n, int parent)
{
	assert(arr != NULL);
	int child = parent * 2 + 1; //parent的左孩子

	while (child < n)
	{
		//如果右孩子存在并且右孩子的值大于左孩子,则让child+1(选出左右孩子中大的那个)
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			child++;
		}

		if (arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);

			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HeapPush(HP* php, HPDataType x)
{
	assert(php != NULL);
	if (php->size == php->capacity)
	{
		int newCapacity = php->capacity == 0 ? 4 : php->capacity * 2;
		HPDataType* tmp = (HPDataType*)realloc(php->arr, newCapacity * sizeof(HPDataType));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			return;
		}
		php->arr = tmp;
		php->capacity = newCapacity;
	}
	php->arr[php->size++] = x;
	
	//向上调整 
	AdjustUp(php->arr, php->size - 1);
}

bool HeapEmpty(HP* php)
{
	assert(php != NULL);

	return php->size == 0;
}

void HeapPop(HP* php)
{
	assert(php != NULL);
	assert(!HeapEmpty(php));
	//删除堆顶的元素,但是删之前要与数组末尾的元素交换,然后让size-1
	Swap(&php->arr[0], &php->arr[php->size - 1]);
	php->size--;
	//向下调整
	AdjustDown(php->arr, php->size, 0);
}

HPDataType HeapTop(HP* php)
{
	assert(php != NULL);
	assert(!HeapEmpty(php));

	return php->arr[0];
}

int HeapSize(HP* php)
{
	assert(php != NULL);

	return php->size;
}

void HeapDestroy(HP* php)
{
	assert(php != NULL);
	free(php->arr);
	php->arr = NULL;
	php->size = php->capacity = 0;
}