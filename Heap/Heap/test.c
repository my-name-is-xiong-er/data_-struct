#include"heap.h"
void HeapTest()
{
	HP hp;
	HeapInit(&hp);
	int arr[] = { 70, 30, 25, 10, 15, 56 };
	for (int i = 0; i < sizeof(arr) / sizeof(int); i++)
	{
		HeapPush(&hp, arr[i]);
	}
	printf("%d\n", HeapSize(&hp));

	while (!HeapEmpty(&hp))
	{
		HPDataType top = HeapTop(&hp);
		printf("%d ", top);
		HeapPop(&hp);
	}

	HeapDestroy(&hp);
}

int main()
{
	HeapTest();
	return 0;
}