#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>

//堆的结构
typedef int HPDataType;
typedef struct Heap
{
	HPDataType* arr;
	int size; //有效数据个数
	int capacity; //空间容量
}HP;

//初始化堆
void HeapInit(HP* php);

//交换两个数
void Swap(HPDataType* p1, HPDataType* p2);

//向上调整算法
void AdjustUp(HPDataType* arr, int child);

//向下调整算法
void AdjustDown(HPDataType* arr, int ArrSize, int parent);

//向堆中插入数据
void HeapPush(HP* php, HPDataType x);

//判断堆是否为空
bool HeapEmpty(HP* php);

//删除堆顶元素
void HeapPop(HP* php);

//返回堆顶元素
HPDataType HeapTop(HP* php);

//堆的大小
int HeapSize(HP* php);

//销毁堆
void HeapDestroy(HP* php);