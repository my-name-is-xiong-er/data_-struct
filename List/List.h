#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
//定义双向链表的节点结构
typedef int LTDataType;
typedef struct ListNode
{
	LTDataType data;
	struct ListNode* next;
	struct ListNode* prev;
}LTNode;

//初始化双向链表
//方法1
void LTInit1(LTNode** pphead);
//方法2
LTNode* LTInit2();

//打印链表数据
void LTPrint(LTNode* phead);

//链表的尾插
void LTPushBack(LTNode* phead, LTDataType x);

//链表的头插
void LTPushFront(LTNode* phead, LTDataType x);

//链表的尾删
void LTPopBack(LTNode* phead);

//链表的头删
void LTPopFront(LTNode* phead);

//链表数据的查找
LTNode* LTFind(LTNode* phead, LTDataType x);

//在指定位置之后插入节点
void LTInsertAfter(LTNode* pos, LTDataType x);

//删除指定位置的节点
void LTErase(LTNode* pos);

//销毁链表
void LTDestory(LTNode* phead);