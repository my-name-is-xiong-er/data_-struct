#define _CRT_SECURE_NO_WARNINGS 1
#include"List.h"
void ListTest01()
{
	LTNode* plist = NULL;
	//初始化链表(链表中只有一个哨兵位)
	LTInit1(&plist);
	//LTNode* plist = LTInit2();
	//尾插节点
	LTPushBack(plist, 1);
	LTPrint(plist);
	LTPushBack(plist, 2);
	LTPrint(plist);
	LTPushBack(plist, 3);
	LTPrint(plist);
	//头插节点
	LTPushFront(plist, 4);
	LTPrint(plist);
	LTPushFront(plist, 5);
	LTPrint(plist);
	//尾删节点
	LTPopBack(plist);
	LTPrint(plist);
	LTPopBack(plist);
	LTPrint(plist);
	//头删节点
	LTPopFront(plist);
	LTPrint(plist);
	LTPopFront(plist);
	LTPrint(plist);
	LTPopFront(plist);
	LTPrint(plist);
	//查找链表中的数据
	LTNode* find = LTFind(plist, 1);
	if (find == NULL)
	{
		printf("没找到!\n");
	}
	else
	{
		printf("找到了!\n");
	}
	//在指定位置之后插入数据
	LTInsertAfter(find, 66);
	LTPrint(plist);
	//删除指定位置的节点
	LTErase(find);
	find = NULL;
	//销毁链表
	LTDestory(plist);
	plist = NULL;
}
int main()
{
	ListTest01();
	return 0;
}