#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

//定义节点的结构
typedef int QDataType;
typedef struct QueueNode
{
	QDataType data;
	struct QueueNode* next;
}QNode;

//队列的结构
typedef struct Queue
{
	QNode* phead; //队头指针
	QNode* ptail; //队尾指针
	int size; //队列元素个数
}Queue;

//队列初始化
void QueueInit(Queue* pq);
//判断队列是否为空
bool QEmpty(Queue* pq);
//入队
void QueuePush(Queue* pq, QDataType x);
//出队
void QueuePop(Queue* pq);
//返回队头元素数据
QDataType QueueFront(Queue* pq);
//返回队尾元素数据
QDataType QueueBack(Queue* pq);
//返回队列的元素(节点)个数
int QueueSize(Queue* pq);
//销毁队列
void QueueDestroy(Queue* pq);