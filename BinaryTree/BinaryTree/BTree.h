#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

typedef char BTDataType;
//二叉链
typedef struct BinaryTreeNode
{
	struct BinaryTreeNode* left; // 指向当前结点的左孩子
	struct BinaryTreeNode* right; // 指向当前结点的右孩子
	BTDataType val; // 当前结点的值域
}BTNode;

//申请节点
BTNode* BuyBTNode(int val);
//创建二叉树
BTNode* CreateBTree();
//前序遍历
void PreOrder(BTNode* root);
//中续遍历
void InOrder(BTNode* root);
//后序遍历
void PostOrder(BTNode* root);
//二叉树的节点个数
int BTreeSize(BTNode* root);
//二叉树的叶子节点个数
int BTreeLeafSize(BTNode* root);
//第K层的节点个数
int BTreeLevelKSize(BTNode* root, int k);
//二叉树的高度/深度
int BTreeDepth(BTNode* root);
//查找指定的值x
BTNode* BTreeFind(BTNode* root, BTDataType x);
//二叉树的销毁
void BTreeDestroy(BTNode* root);