#include"BTree.h"

BTNode* CreateBTree()
{
	BTNode* node1 = BuyBTNode('A');
	BTNode* node2 = BuyBTNode('B');
	BTNode* node3 = BuyBTNode('D');
	BTNode* node4 = BuyBTNode('C');
	BTNode* node5 = BuyBTNode('E');
	BTNode* node6 = BuyBTNode('F');
	BTNode* node7 = BuyBTNode('G');

	node1->left = node2;
	node1->right = node4;
	node2->left = node3;
	node2->right = node7;
	node4->left = node5;
	node4->right = node6;

	return node1;
}

void BTreeTest()
{
	BTNode* root = CreateBTree();
	printf("前序遍历:");
	PreOrder(root);
	printf("\n");

	printf("中序遍历:");
	InOrder(root);
	printf("\n");

	printf("后序遍历:");
	PostOrder(root);
	printf("\n");

	printf("nodeSize:%d\n", BTreeSize(root));
	printf("leafSize:%d\n", BTreeLeafSize(root));
	printf("第3层的节点个数:%d\n", BTreeLevelKSize(root,3));
	printf("BTreeHight:%d\n", BTreeDepth(root));
	
	BTNode* find = BTreeFind(root, 'G');
	if (find != NULL)
	{
		printf("找到了,节点地址为:%p\n", find);
	}
	else
	{
		printf("没有找到!\n");
	}

	BTreeDestroy(root);
	root = NULL;
}

int main()
{
	BTreeTest();
	return 0;
}