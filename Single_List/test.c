#define _CRT_SECURE_NO_WARNINGS 1
#include"SList.h"
void SListTest01()
{
	//手动创建四个节点并将它们连起来
	//SLTNode* node1 = (SLTNode*)malloc(sizeof(SLTNode));
	//node1->data = 1;
	//SLTNode* node2 = (SLTNode*)malloc(sizeof(SLTNode));
	//node2->data = 2;
	//SLTNode* node3 = (SLTNode*)malloc(sizeof(SLTNode));
	//node3->data = 3;
	//SLTNode* node4 = (SLTNode*)malloc(sizeof(SLTNode));
	//node4->data = 4;
	//node1->next = node2;
	//node2->next = node3;
	//node3->next = node4;
	//node4->next = NULL;
	//SLTNode* plist = node1;
	//SLTPrint(plist);

	//创建一个结构体(节点结构)指针用于后续链表节点的插入
	SLTNode* plist = NULL;
	//①尾插节点
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist);

	//②头插节点
	SLTPushFront(&plist, 6);
	SLTPrint(plist);
	SLTPushFront(&plist, 7);
	SLTPrint(plist);
	SLTPushFront(&plist, 8);
	SLTPrint(plist);

	//③查找链表中的数据
	SLTNode* find = SLTFind(plist, 2);
	//if (find == NULL)
	//{
	//	printf("没有找到!\n");
	//}
	//else
	//{
	//	printf("找到了!\n");
	//}

	//④在指定位置之前插入数据
	SLTInsertPre(&plist, find, 11);
	SLTPrint(plist);

	//⑤在指定位置之后插入数据
	SLTInsertAfter(find, 22);
	SLTPrint(plist);

	////⑥尾删节点
	//SLTPopBack(&plist);
	//SLTPrint(plist);
	//SLTPopBack(&plist);
	//SLTPrint(plist);
	//SLTPopBack(&plist);
	//SLTPrint(plist);
	//SLTPopBack(&plist);
	//SLTPrint(plist);

	//⑦头删节点
	SLTPopFront(&plist);
	SLTPrint(plist);

	////⑧删除指定位置的节点
	//SLTErase(&plist, find);
	//SLTPrint(plist);

	//⑨删除指定位置之后的节点
	SLTEraseAfter(find);
	find = NULL;
	SLTPrint(plist);

	//⑩销毁链表
	SLTDestory(&plist);
	SLTPrint(plist);
}
int main()
{
	SListTest01();
	return 0;
}