#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
//定义节点的结构
typedef int SLTDataType;
//数据+指向下一个节点的指针
typedef struct SListNode
{
	SLTDataType data;
	struct SListNode* next;
}SLTNode;

//打印链表中的数据
void SLTPrint(SLTNode* phead);

//链表尾插节点
void SLTPushBack(SLTNode** pphead, SLTDataType x);

//链表头插节点
void SLTPushFront(SLTNode** pphead, SLTDataType x);

//链表的尾删
void SLTPopBack(SLTNode** pphead);

//链表的头删
void SLTPopFront(SLTNode** pphead);

//链表数据的查找
SLTNode* SLTFind(SLTNode* phead, SLTDataType x);

//在指定位置之前插入数据
void SLTInsertPre(SLTNode** pphead, SLTNode* pos, SLTDataType x);

//在指定位置之后插入数据
void SLTInsertAfter(SLTNode* pos, SLTDataType x);

//删除指定位置的节点
void SLTErase(SLTNode** pphead, SLTNode* pos);

//删除指定位置之后的节点
void SLTEraseAfter(SLTNode* pos);

//链表的销毁
void SLTDestory(SLTNode** pphead);