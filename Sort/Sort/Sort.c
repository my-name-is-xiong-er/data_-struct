#include"Sort.h"
#include"Stack.h"

void PrintArr(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

void InsertSort(int* arr, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		//[0,end]之间的数据有序,则让tmp插入以后依旧有序
		int end = i;
		int tmp = arr[i + 1];
		while (end >= 0)
		{
			if (arr[end] > tmp)
			{
				arr[end + 1] = arr[end];
				end--;
			}
			else
			{
				break;
			}
		}
		arr[end + 1] = tmp;
	}
}

void ShellSort(int* arr, int n)
{
	//1.gap>1 预排序
	//2.gap==1 直接插入排序
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;  //+1是保证让gap最后能走到1
		//gap = gap/2;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > tmp)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else
				{
					break;
				}
			}
			arr[end + gap] = tmp;
		}
	}
}

void Swap(int* x, int* y)
{
	int tmp = *x;
	*x = *y;
	*y = tmp;
}

void SelectSort(int* arr, int n)
{
	int begin = 0, end = n - 1;
	while (begin < end)
	{
		int maxi = begin, mini = begin;
		for (int i = begin; i <= end; i++)
		{
			if (arr[i] < arr[mini])
			{
				mini = i;
			}
			if (arr[i] > arr[maxi])
			{
				maxi = i;
			}
		}
		Swap(&arr[begin], &arr[mini]);
		//如果maxi就在begin位置就修正一下maxi
		if (maxi == begin)
			maxi = mini;

		Swap(&arr[end], &arr[maxi]);
		begin++;
		end--;
	}
}

//向下调整算法
void AdjustDown(int* arr, int n, int parent)
{
	assert(arr != NULL);
	int child = parent * 2 + 1; //parent的左孩子

	while (child < n)
	{
		//如果右孩子存在并且右孩子的值小于左孩子,则让child+1(选出小的那个)
		if (child + 1 < n && arr[child + 1] > arr[child])
		{
			child++;
		}

		if (arr[child] > arr[parent])
		{
			Swap(&arr[child], &arr[parent]);

			parent = child;
			child = parent * 2 + 1;
		}
		else
		{
			break;
		}
	}
}
void HeapSort(int* arr, int n)
{
	//建堆--向下调整建堆
	for (int i = (n - 1 - 1) / 2; i >= 0; --i)
	{
		AdjustDown(arr, n, i);
	}

	int end = n - 1;
	while (end > 0)
	{
		Swap(&arr[0], &arr[end]);
		AdjustDown(arr, end, 0);
		end--;
	}
}

void BubbleSort(int* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		bool exchange = false;
		for (int j = 0; j < n - 1 - i; j++)
		{
			//若前面的数据比后面的大则交换
			if (arr[j] > arr[j + 1])
			{
				Swap(&arr[j], &arr[j + 1]);
				exchange = true;
			}
		}
		//若在一趟冒泡排序中没有发生数据的交换,则说明数据已经有序
		if (exchange == false)
		{
			break;
		}
	}
}

int GetMidIndex(int* arr, int left, int right)
{
	int midi = (left + right) / 2;
	//int midi = left + (rand() % (right - left));
	if (arr[left] > arr[midi])
	{
		if (arr[midi] > arr[right])
		{
			return midi;
		}
		else if (arr[left] > arr[right])
		{
			return right;
		}
		else
		{
			return left;
		}
	}
	else
	{
		if (arr[right] > arr[midi])
		{
			return midi;
		}
		else if (arr[left] > arr[right])
		{
			return left;
		}
		else
		{
			return right;
		}
	}
}

int PartSort1(int* arr, int left, int right)
{
	int midi = GetMidIndex(arr, left, right);
	Swap(&arr[left], &arr[midi]);

	int keyi = left;
	while (left < right)
	{
		while (left < right && arr[right] >= arr[keyi])
		{
			right--;
		}
		while (left < right && arr[left] <= arr[keyi])
		{
			left++;
		}

		if (left < right)
			Swap(&arr[left], &arr[right]);
	}

	if (keyi != left)
		Swap(&arr[keyi], &arr[left]);

	return left;
}

int PartSort2(int* arr, int left, int right)
{
	int mid = GetMidIndex(arr, left, right);
	Swap(&arr[mid], &arr[left]);

	int key = arr[left];
	int hole = left;
	while (left < right)
	{
		while (left < right && arr[right] >= key)
		{
			right--;
		}
		arr[hole] = arr[right];
		hole = right;

		while (left < right && arr[left] <= key)
		{
			left++;
		}
		arr[hole] = arr[left];
		hole = left;
	}
	arr[hole] = key;

	return hole;
}

int PartSort3(int* arr, int left, int right)
{
	int mid = GetMidIndex(arr, left, right);
	Swap(&arr[mid], &arr[left]);

	int prev = left;
	int cur = left + 1;
	int keyi = left;
	while (cur <= right)
	{
		if (arr[cur] < arr[keyi] && ++prev != cur)
		{
			Swap(&arr[cur], &arr[prev]);
		}
		cur++;
	}
	Swap(&arr[prev], &arr[keyi]);
	keyi = prev;
	return keyi;
}
//二路划分
void QuickSort1(int* arr, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}
	int keyi = PartSort2(arr, begin, end);
	//区间:[begin, keyi-1] keyi [keyi+1,end]
	QuickSort1(arr, begin, keyi - 1);
	QuickSort1(arr, keyi + 1, end);
}
//三路划分
void QuickSort2(int* arr, int begin, int end)
{
	if (begin >= end)
	{
		return;
	}
	int left = begin;
	int right = end;
	int cur = left + 1;

	int mid = GetMidIndex(arr, left, right);
	Swap(&arr[mid], &arr[left]);
	int key = arr[left];

	while (cur <= right)
	{
		if (arr[cur] < key)
		{
			Swap(&arr[cur], &arr[left]);
			cur++;
			left++;
		}
		else if (arr[cur] > key)
		{
			Swap(&arr[cur], &arr[right]);
			right--;
		}
		else
		{
			cur++;
		}
	}
	//到此区间被划分为[begin,left-1][left,right][right+1,end]
	QuickSort2(arr, begin, left - 1);
	QuickSort2(arr, right + 1, end);
}

void QuickSortNonR(int* arr, int left, int right)
{
	ST st;
	STInit(&st);
	STPush(&st, right);
	STPush(&st, left);

	while (!STEmpty(&st))
	{
		int left = STTop(&st);
		STPop(&st);
		int right = STTop(&st);
		STPop(&st);

		int keyi = PartSort1(arr, left, right);
		//[left,keyi-1] keyi [keyi+1,right]

		if (keyi - 1 > left)
		{
			STPush(&st, keyi - 1);
			STPush(&st, left);
		}
		if (right > keyi + 1)
		{
			STPush(&st, right);
			STPush(&st, keyi + 1);
		}
	}
	STDestroy(&st);
}

void _MergeSort(int* arr, int begin, int end, int* tmp)
{
	if (begin == end)
		return;

	//小区间优化
	if (end - begin + 1 < 10)
	{
		InsertSort(arr + begin, end - begin + 1);
		return;
	}

	int mid = (begin + end) / 2;
	//区间被分为:[begin,mid][mid+1,end]
	_MergeSort(arr, begin, mid, tmp);
	_MergeSort(arr, mid + 1, end, tmp);

	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int i = begin;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
		{
			tmp[i++] = arr[begin1++];
		}
		else
		{
			tmp[i++] = arr[begin2++];
		}
	}

	while (begin1 <= end1)
	{
		tmp[i++] = arr[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[i++] = arr[begin2++];
	}
	memcpy(arr + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

void MergeSort(int* arr, int n)
{
	int* tmp = (int*)malloc(n * sizeof(int));
	if (tmp == NULL)
	{
		perror("malloc fail!");
		return;
	}

	_MergeSort(arr, 0, n - 1, tmp);
	free(tmp);
}

void MergeSortNonR1(int* arr, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail!");
		return;
	}
	int gap = 1;
	while (gap < n)
	{
		int i = 0;
		for (int j = 0; j < n; j += 2 * gap)
		{
			int begin1 = j, end1 = j + gap - 1;
			int begin2 = j + gap, end2 = j + 2 * gap - 1;
			//printf("[%d,%d][%d,%d]\n", begin1, end1, begin2, end2);

			if (end1 >= n || begin2 >= n)
			{
				break;
			}
			//修正end2
			if (end2 >= n)
			{
				end2 = n - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2])
				{
					tmp[i++] = arr[begin1++];
				}
				else
				{
					tmp[i++] = arr[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[i++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[i++] = arr[begin2++];
			}
			//归并一组,拷贝一组
			memcpy(arr + j, tmp + j, sizeof(int) * (end2 - j + 1));
		}
		gap *= 2;
		//printf("\n");
	}
	free(tmp);
}

void MergeSortNonR2(int* arr, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail!");
		return;
	}
	int gap = 1;
	while (gap < n)
	{
		int i = 0;
		for (int j = 0; j < n; j += 2 * gap)
		{
			int begin1 = j, end1 = j + gap - 1;
			int begin2 = j + gap, end2 = j + 2 * gap - 1;
			//printf("修正前:[%d,%d][%d,%d]\n", begin1, end1, begin2, end2);

			if (end1 >= n)
			{
				end1 = n - 1;
				//改成不存在的区间
				begin2 = n;
				end2 = n - 1;
			}
			else if (begin2 >= n)
			{
				//改成不存在的区间
				begin2 = n;
				end2 = n - 1;
			}
			else if (end2 >= n)
			{
				end2 = n - 1;
			}
			//printf("修正后:[%d,%d][%d,%d]\n", begin1, end1, begin2, end2);

			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2])
				{
					tmp[i++] = arr[begin1++];
				}
				else
				{
					tmp[i++] = arr[begin2++];
				}
			}
			while (begin1 <= end1)
			{
				tmp[i++] = arr[begin1++];
			}
			while (begin2 <= end2)
			{
				tmp[i++] = arr[begin2++];
			}
		}
		//整组拷贝
		memcpy(arr, tmp, sizeof(int) * n);
		gap *= 2;
		//printf("\n");
	}
	free(tmp);
}

void CountSort(int* arr, int n)
{
	int min = arr[0], max = arr[0];
	for (int i = 0; i < n; i++)
	{
		if (arr[i] < min)
		{
			min = arr[i];
		}
		if (arr[i] > max)
		{
			max = arr[i];
		}
	}
	int range = max - min + 1;
	int* CountA = (int*)malloc(sizeof(int) * range);
	if (CountA == NULL)
	{
		perror("malloc fail!");
		return;
	}
	//初始化CountA数组为全0
	memset(CountA, 0, sizeof(int) * range);

	//计数(相对映射)
	for (int i = 0; i < n; i++)
	{
		CountA[arr[i] - min]++;
	}
	//排序
	int k = 0;
	for (int j = 0; j < range; j++)
	{
		while (CountA[j]--)
		{
			arr[k++] = j + min;
		}
	}
	free(CountA);
}