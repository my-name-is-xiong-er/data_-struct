#include"Stack.h"

//初始化栈
void STInit(ST* pst)
{
	assert(pst != NULL);
	pst->arr = NULL;
	//pst->top = -1; //top指向栈顶元素
	pst->top = 0; //top指向栈顶元素的下一个位置
	pst->capacity = 0;
}

//销毁栈
void STDestroy(ST* pst)
{
	assert(pst != NULL);
	free(pst->arr);
	pst->arr = NULL;
	pst->top = pst->capacity = 0;
}

//判断栈里是否为空
bool STEmpty(ST* pst)
{
	assert(pst != NULL);

	return pst->top == 0;
}

//入栈
void STPush(ST* pst, STDataType x)
{
	assert(pst != NULL);
	if (pst->top == pst->capacity)
	{
		int newCapacity = pst->capacity == 0 ? 4 : pst->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(pst->arr, newCapacity * sizeof(STDataType));
		if (tmp == NULL)
		{
			perror("realloc fail!");
			return;
		}
		pst->arr = tmp;
		pst->capacity = newCapacity;
	}

	pst->arr[pst->top++] = x;
}

//出栈
void STPop(ST* pst)
{
	assert(pst != NULL);
	assert(!STEmpty(pst));

	pst->top--;
}

//返回栈顶元素
STDataType STTop(ST* pst)
{
	assert(pst != NULL);
	assert(!STEmpty(pst));

	return pst->arr[pst->top - 1];
}

int STSize(ST* pst)
{
	assert(pst != NULL);
	return pst->top;
}