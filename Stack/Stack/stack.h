#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>
#include<Windows.h>

typedef int STDataType;
typedef struct Stack
{
	STDataType* arr;
	int top; 
	int capacity; //空间容量
}ST;

//初始化栈
void STInit(ST* pst);
//判断栈是否为空
bool STEmpty(ST* pst);
//压栈(栈顶入数据)
void STPush(ST* pst, STDataType x);
//出栈(栈顶出数据)
void STPop(ST* pst);
//返回栈顶元素数据
STDataType STTop(ST* pst);
//返回栈中有效数据个数
int STSize(ST* pst);
//销毁栈
void STDestroy(ST* pst);